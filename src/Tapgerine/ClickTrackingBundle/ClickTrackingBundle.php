<?php

namespace Tapgerine\ClickTrackingBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Tapgerine\ClickTrackingBundle\DependencyInjection\Extension;

/**
 * @author Oleg Postupalsky <work.throyan@gmail.com>
 */
class ClickTrackingBundle extends Bundle
{
    /**
     * @return Extension
     */
    public function getContainerExtension()
    {
        return new Extension();
    }
}
