<?php

namespace Tapgerine\ClickTrackingBundle\Tests\Entity;

use Tapgerine\ClickTrackingBundle\Entity\Click;

/**
 * Click entity test
 */
class ClickTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @param string $ua
     * @param string $ip
     * @param string $ref
     * @param string $p1
     * @param string $p2
     * @param string $id
     *
     * @dataProvider getClicks
     */
    public function testConstructor($ua, $ip, $ref, $p1, $p2, $id)
    {
        $click = new Click($ua, $ip, $ref, $p1, $p2);

        $this->assertEquals($ua, $click->getUserAgent());
        $this->assertEquals($ip, $click->getIpAddress());
        $this->assertEquals($ref, $click->getReferer());
        $this->assertEquals($p1, $click->getParam1());
        $this->assertEquals($p2, $click->getParam2());
        $this->assertEquals($id, $click->getIdentifier());
    }

    /**
     * @return array
     */
    public function getClicks()
    {
        return [
            [
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36',
                '54.246.83.212',
                'http://symfony.com/doc/current/bundles/DoctrineMigrationsBundle/index.html',
                'param1',
                'param2',
                '691560068b63c1867bd35958467a10fd',
            ],
            [
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36',
                '172.217.20.206',
                'https://translate.google.com.ua/?source=osdd',
                'param1',
                'param2',
                'c89544b6e1f8ff8171dc8407cd5a0667',
            ],
            [
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36',
                '172.217.20.206',
                'https://translate.google.com.ua/?source=osdd',
                'param1',
                'this value should not affect hash',
                'c89544b6e1f8ff8171dc8407cd5a0667',
            ],
            [
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36',
                '172.217.20.206',
                'https://translate.google.com.ua/?source=osdd',
                'but this one should',
                'this value should not affect hash',
                '62caa32d10da88f103a0d9c611e3e726',
            ],
        ];
    }

    /**
     * test id entropy
     */
    public function testIdEntropy()
    {
        $identifiers = [];
        for ($i = 0; $i < 1000; $i++) {
            $click = new Click(
                md5(time()),
                long2ip(time()),
                'referer'.$i,
                md5($i),
                'doesn\'t matter'
            );

            $identifiers[] = $click->getIdentifier();
        }

        $this->assertEquals(count($identifiers), count(array_unique($identifiers)));
    }

    /**
     * Test error count increment function
     */
    public function testTriggerError()
    {
        $click = new Click('ua', 'ip', 'ref', 'param1', 'param2');

        $this->assertEquals(0, $click->getErrorCount());
        $click->triggerError();
        $this->assertEquals(1, $click->getErrorCount());
        $click->triggerError();
        $click->triggerError();
        $this->assertEquals(3, $click->getErrorCount());
    }

    /**
     * Test error count increment function
     */
    public function testMarkRefererAsBad()
    {
        $click = new Click('ua', 'ip', 'ref', 'param1', 'param2');

        $this->assertFalse($click->isBadRefererDomain());
        $click->markRefererAsBad();
        $this->assertTrue($click->isBadRefererDomain());
        $click->markRefererAsBad();
        $click->markRefererAsBad();
        $this->assertTrue($click->isBadRefererDomain());
    }
}
