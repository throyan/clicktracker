<?php

namespace Tapgerine\ClickTrackingBundle\Tests\Entity;

use Tapgerine\ClickTrackingBundle\Entity\BadDomain;

/**
 * BadDomaine entity test
 */
class BadDomainTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test creating object
     *
     * @param string $name
     * @param string $expectedName
     *
     * @dataProvider getDomains
     */
    public function testConstructor($name, $expectedName)
    {
        $domain = new BadDomain($name);

        $this->assertEquals($expectedName, $domain->getName());
        $this->assertNull($domain->getIdentifier());
    }

    /**
     * @return array
     */
    public function getDomains()
    {
        return [
            ['http://google.com', 'google.com'],
            ['https://www.stackoverflow.com', 'www.stackoverflow.com'],
        ];
    }
}
