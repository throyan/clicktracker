<?php

namespace Tapgerine\ClickTrackingBundle\Tests\Tracker;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Tapgerine\ClickTrackingBundle\Entity\BadDomain;
use Tapgerine\ClickTrackingBundle\Entity\Click;
use Tapgerine\ClickTrackingBundle\Tracker\ClickTracker;

/**
 * ClickTracker test
 */
class ClickTrackerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test succesfull track
     */
    public function testSuccessTrack()
    {
        $id = md5(time());
        $referer = 'http://example.com';
        $domain = 'example.com';

        $repository = $this->prophesize(EntityRepository::class);
        // no domains
        $repository->findOneBy(['name' => $domain])
            ->shouldBeCalledTimes(1)
            ->willReturn(null);

        $click = $this->prophesize(Click::class);
        $click->getIdentifier()
            ->shouldBeCalledTimes(1)
            ->willReturn($id);
        $click->getReferer()
            ->shouldBeCalledTimes(1)
            ->willReturn($referer);

        $entityManager = $this->prophesize(EntityManager::class);
        // no duplicates
        $entityManager->find(Click::class, $id)
            ->shouldBeCalledTimes(1)
            ->willReturn(null);
        $entityManager->getRepository(BadDomain::class)
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $entityManager->persist($click)
            ->shouldBeCalledTimes(1);
        $entityManager->flush($click)
            ->shouldBeCalledTimes(1);

        $clickTracker = new ClickTracker($entityManager->reveal());

        $this->assertTrue($clickTracker->track($click->reveal()));
    }

    /**
     * Test track of duplicated click
     */
    public function testDuplicatedTrack()
    {
        $id = md5(time());
        $referer = 'http://example.com';
        $domain = 'example.com';

        $repository = $this->prophesize(EntityRepository::class);
        $repository->findOneBy(['name' => $domain])
            ->shouldBeCalledTimes(1)
            ->willReturn(null);

        $click = $this->prophesize(Click::class);
        $click->getIdentifier()
            ->shouldBeCalledTimes(1)
            ->willReturn($id);
        $duplicate = $this->prophesize(Click::class);
        // click and duplicate will be switched inside track method
        $duplicate->getReferer()
            ->shouldBeCalledTimes(1)
            ->willReturn($referer);
        $duplicate->triggerError()
            ->shouldBeCalledTimes(1);

        $entityManager = $this->prophesize(EntityManager::class);
        $entityManager->getRepository(BadDomain::class)
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        // now we'll find duplicate
        $entityManager->find(Click::class, $id)
            ->shouldBeCalledTimes(1)
            ->willReturn($duplicate->reveal());
        // save duplicates!
        $entityManager->persist($duplicate)
            ->shouldBeCalledTimes(1);
        $entityManager->flush($duplicate)
            ->shouldBeCalledTimes(1);

        $clickTracker = new ClickTracker($entityManager->reveal());

        $this->assertFalse($clickTracker->track($click->reveal()));
    }

    /**
     * Test track of click from bad domain
     */
    public function testTrackFromBadDomain()
    {
        $id = md5(time());
        $referer = 'http://example.com';
        $domain = 'example.com';

        $repository = $this->prophesize(EntityRepository::class);
        // we'll find bad domain
        $repository->findOneBy(['name' => $domain])
            ->shouldBeCalledTimes(1)
            ->willReturn($this->prophesize(BadDomain::class));

        $click = $this->prophesize(Click::class);
        $click->getIdentifier()
            ->shouldBeCalledTimes(1)
            ->willReturn($id);
        $click->getReferer()
            ->shouldBeCalledTimes(1)
            ->willReturn($referer);
        // mark as error
        $click->triggerError()
            ->shouldBeCalledTimes(1);
        $click->markRefererAsBad()
            ->shouldBeCalledTimes(1);

        $entityManager = $this->prophesize(EntityManager::class);
        $entityManager->find(Click::class, $id)
            ->shouldBeCalledTimes(1)
            ->willReturn(null);
        $entityManager->getRepository(BadDomain::class)
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $entityManager->persist($click)
            ->shouldBeCalledTimes(1);
        $entityManager->flush($click)
            ->shouldBeCalledTimes(1);

        $clickTracker = new ClickTracker($entityManager->reveal());

        $this->assertFalse($clickTracker->track($click->reveal()));
    }

    /**
     * Test track of duplicated click from bad domain
     */
    public function testDuplicatedTrackFromBadDomain()
    {
        $id = md5(time());
        $referer = 'http://example.com';
        $domain = 'example.com';

        $repository = $this->prophesize(EntityRepository::class);
        $repository->findOneBy(['name' => $domain])
            ->shouldBeCalledTimes(1)
            ->willReturn($this->prophesize(BadDomain::class));

        $click = $this->prophesize(Click::class);
        $click->getIdentifier()
            ->shouldBeCalledTimes(1)
            ->willReturn($id);
        $duplicate = $this->prophesize(Click::class);
        $duplicate->getReferer()
            ->shouldBeCalledTimes(1)
            ->willReturn($referer);
        // 2 error at once (!)
        $duplicate->triggerError()
            ->shouldBeCalledTimes(2);
        $duplicate->markRefererAsBad()
            ->shouldBeCalledTimes(1);

        $entityManager = $this->prophesize(EntityManager::class);
        $entityManager->getRepository(BadDomain::class)
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        // now we'll find duplicate
        $entityManager->find(Click::class, $id)
            ->shouldBeCalledTimes(1)
            ->willReturn($duplicate->reveal());
        // save duplicates!
        $entityManager->persist($duplicate)
            ->shouldBeCalledTimes(1);
        $entityManager->flush($duplicate)
            ->shouldBeCalledTimes(1);

        $clickTracker = new ClickTracker($entityManager->reveal());

        $this->assertFalse($clickTracker->track($click->reveal()));
    }
}
