<?php

namespace Tapgerine\ClickTrackingBundle\Tracker;

use Doctrine\ORM\EntityManager;
use Tapgerine\ClickTrackingBundle\Entity\BadDomain;
use Tapgerine\ClickTrackingBundle\Entity\Click;

/**
 * Click tracker
 *
 * @author Oleg Postupalsky <work.throyan@gmail.com>
 */
class ClickTracker
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Click $click
     *
     * @return bool
     */
    public function track(Click $click)
    {
        $result = true;

        // check for duplicate
        /** @var Click $duplicate */
        $duplicate = $this->entityManager->find(Click::class, $click->getIdentifier());
        if ($duplicate !== null) {
            $click = $duplicate;
            $click->triggerError();

            $result = false;
            // should we?
            // return false;
        }

        // check for bad domain
        $domain = parse_url($click->getReferer(), PHP_URL_HOST);

        $badDomainRepository = $this->entityManager->getRepository(BadDomain::class);

        $badDomain = $badDomainRepository->findOneBy(['name' => $domain]);

        if ($badDomain !== null) {
            $click->triggerError();
            $click->markRefererAsBad();

            $result = false;
        }

        $this->entityManager->persist($click);
        $this->entityManager->flush($click);

        return $result;
    }
}
