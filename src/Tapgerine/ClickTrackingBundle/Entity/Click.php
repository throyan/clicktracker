<?php

namespace Tapgerine\ClickTrackingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Click information
 *
 * @author Oleg Postupalsky <work.throyan@gmail.com>
 *
 * @ORM\Entity()
 * @ORM\Table("click")
 */
class Click
{
    /**
     * @var string
     *
     * @ORM\Column(name="id",type="string")
     * @ORM\Id
     */
    protected $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="ua",type="string")
     */
    protected $userAgent;

    /**
     * @var string
     *
     * @ORM\Column(name="ip",type="string")
     */
    protected $ipAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="ref",type="string")
     */
    protected $referer;

    /**
     * @var string
     *
     * @ORM\Column(name="param1",type="string")
     */
    protected $param1;

    /**
     * @var string
     *
     * @ORM\Column(name="param2",type="string")
     */
    protected $param2;

    /**
     * @var string
     *
     * @ORM\Column(name="error",type="string")
     */
    protected $errorCount = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="bad_domain",type="boolean")
     */
    protected $badRefererDomain = false;

    /**
     * @param string $userAgent
     * @param string $ipAddress
     * @param string $referer
     * @param string $param1
     * @param string $param2
     */
    public function __construct($userAgent, $ipAddress, $referer, $param1, $param2)
    {
        $this->userAgent = $userAgent;
        // @todo: check ip?
        $this->ipAddress = $ipAddress;
        $this->referer = $referer;
        $this->param1 = $param1;
        $this->param2 = $param2;

        $this->identifier = $this->generateIdentifier();
    }

    /**
     * Gets the value of identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Gets the value of userAgent.
     *
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Gets the value of ipAddress.
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Gets the value of referer.
     *
     * @return string
     */
    public function getReferer()
    {
        return $this->referer;
    }

    /**
     * Gets the value of param1.
     *
     * @return string
     */
    public function getParam1()
    {
        return $this->param1;
    }

    /**
     * Gets the value of param2.
     *
     * @return string
     */
    public function getParam2()
    {
        return $this->param2;
    }

    /**
     * Gets the value of errorCount.
     *
     * @return string
     */
    public function getErrorCount()
    {
        return $this->errorCount;
    }

    /**
     * Gets the value of badRefererDomain.
     *
     * @return bool
     */
    public function isBadRefererDomain()
    {
        return $this->badRefererDomain;
    }

    /**
     * @return self
     */
    public function triggerError()
    {
        $this->errorCount++;

        return $this;
    }

    /**
     * @return self
     */
    public function markRefererAsBad()
    {
        $this->badRefererDomain = true;

        return $this;
    }

    /**
     * @return string
     */
    protected function generateIdentifier()
    {
        // @todo: test entropy
        return md5($this->userAgent.'-'.$this->ipAddress.'-'.$this->referer.'-'.$this->param1);
    }
}
