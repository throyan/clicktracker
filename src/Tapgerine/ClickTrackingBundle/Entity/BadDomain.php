<?php

namespace Tapgerine\ClickTrackingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bad domain record
 *
 * @author Oleg Postupalsky <work.throyan@gmail.com>
 *
 * @ORM\Entity()
 * @ORM\Table("bad_domain")
 */
class BadDomain
{
    /**
     * @var string
     *
     * @ORM\Column(name="id",type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="name",type="string",unique=true)
     */
    protected $name;

    /**
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = parse_url($name, PHP_URL_HOST);
    }

    /**
     * Gets the value of identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Gets the value of name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
