<?php

namespace Tapgerine\ClickTrackingBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tapgerine\ClickTrackingBundle\Entity\Click;
use Tapgerine\ClickTrackingBundle\Tracker\ClickTracker;

/**
 * Click tracking controller
 *
 * @author Oleg Postupalsky <work.throyan@gmail.com>
 */
class ClicksController extends Controller
{
    /**
     * @Route("/", name="click_list")
     *
     * @return Response
     */
    public function indexAction()
    {
        $entityManager = $this->getDoctrine()->getEntityManager();
        $repository = $entityManager->getRepository(Click::class);

        $clicks = $repository->findAll();

        return $this->render('ClickTrackingBundle:Clicks:list.html.twig', [
            'clicks' => $clicks,
        ]);
    }

    /**
     * @Route("/click", name="click")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function clickAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getEntityManager();
        /** @var ClickTracker $clickTracker */
        $clickTracker = $this->get('click_tracker');

        $headers = $request->headers;
        $userAgent = $headers->get('user-agent', '');
        $ipAddress = $request->getClientIp();
        $referer = $headers->get('referer', '');
        $param1 = $request->get('param1', '');
        $param2 = $request->get('param2', '');

        $click = new Click($userAgent, $ipAddress, $referer, $param1, $param2);

        if ($clickTracker->track($click)) {
            return $this->redirectToRoute('click_success', ['id' => $click->getIdentifier()]);
        }

        return $this->redirectToRoute('click_error', ['id' => $click->getIdentifier()]);
    }

    /**
     * @Route("/succes/{id}", name="click_success")
     *
     * @param int $id
     *
     * @return Response
     */
    public function successAction($id)
    {
        $entityManager = $this->getDoctrine()->getEntityManager();

        $click = $entityManager->find(Click::class, $id);

        if ($click === null) {
            throw $this->createNotFoundException('Tracking information not found');
        }

        return $this->render('ClickTrackingBundle:Clicks:success.html.twig', [
            'click' => $click,
        ]);
    }

    /**
     * @Route("/error/{id}", name="click_error")
     *
     * @param int $id
     *
     * @return Response
     */
    public function errorAction($id)
    {
        $entityManager = $this->getDoctrine()->getEntityManager();

        $click = $entityManager->find(Click::class, $id);

        if ($click === null) {
            throw $this->createNotFoundException('Tracking information not found');
        }

        return $this->render('ClickTrackingBundle:Clicks:error.html.twig', [
            'click' => $click,
            'redirectLink' => 'https://google.com',
        ]);
    }
}
