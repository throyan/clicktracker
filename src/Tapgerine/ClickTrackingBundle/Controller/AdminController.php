<?php

namespace Tapgerine\ClickTrackingBundle\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tapgerine\ClickTrackingBundle\Entity\BadDomain;

/**
 * Controller to manage data
 *
 * @author Oleg Postupalsky <work.throyan@gmail.com>
 */
class AdminController extends Controller
{
    /**
     * @Route("/admin", name="admin")
     *
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('ClickTrackingBundle:Admin:index.html.twig');
    }

    /**
     * @Route("/admin/bad-domains", name="admin_baddomains_list")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function listBadDomainsAction(Request $request)
    {
        /** @var EntityRepository $domainRepository */
        $domainRepository = $this->getDoctrine()->getManager()->getRepository(BadDomain::class);

        $domains = $domainRepository->findAll();

        return $this->render('ClickTrackingBundle:Admin:bad-domains/list.html.twig', [
            'message' => $request->get('message'),
            'error' => $request->get('error'),
            'badDomains' => $domains,
        ]);
    }

    /**
     * @Route("/admin/bad-domains/add", name="admin_baddomains_add")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Bad Domain Record'))
            ->getForm();

        $form->handleRequest($request);

        // save new record
        if ($form->isSubmitted() && $form->isValid()) {
            // collect data
            $data = $form->getData();
            $badDomain = new BadDomain($data['name']);

            // try to save
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($badDomain);
            try {
                $entityManager->flush($badDomain);
                // succesfully created
                $message = $badDomain->getName().' succesfully created';

                return $this->redirectToRoute('admin_baddomains_list', ['message' => $message]);
            } catch (UniqueConstraintViolationException $e) {
                // already exists
                $error = $badDomain->getName().' already exists';

                $form->get('name')->addError(new FormError($error));
            }
        }

        return $this->render('ClickTrackingBundle:Admin:bad-domains/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/bad-domains/delete/{id}", name="admin_baddomains_delete")
     *
     * @param int $id
     *
     * @return Response
     */
    public function deleteAction($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var BadDomain $badDomain */
        $badDomain = $entityManager->find(BadDomain::class, $id);

        if (! $badDomain) {
            $error = 'Domain '.$id.' not found';

            return $this->redirectToRoute('admin_baddomains_list', ['error' => $error]);
        }

        $entityManager->remove($badDomain);
        $entityManager->flush($badDomain);

        $message = $badDomain->getName().' succesfully deleted';

        return $this->redirectToRoute('admin_baddomains_list', ['message' => $message]);
    }
}
